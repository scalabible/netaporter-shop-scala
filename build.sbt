name := "naptest"

organization := "com.netaporter"
     
version := "0.1-SNAPSHOT"

scalaVersion := "2.11.7"

//
val testDependencies = Seq (
  "org.specs2"                 %% "specs2-core"                     % "3.6.6",
  "org.specs2"                 %% "specs2-mock"                     % "3.6.6"
)

val rootDependencies = Seq(
  "org.typelevel"             %% "cats"                         % "0.6.0"
)

val dependencies = testDependencies ++ rootDependencies
libraryDependencies ++= dependencies

