package com.netaporter.repositories

import cats.{Now, Monad}
import scala.collection.mutable

trait RepositoryEnvironment[M[_]] {
  def M: Monad[M]
  def insert[A](key:String,  value: A): M[Unit]
  def get[A](key: String): M[Option[A]]
  def delete(key: String): M[Unit]
  def put[A](key: String, value: A): M[Unit]
  def listAll[A]:M[List[A]]
}

case class NetaporterRepositoryEnvironment[M[_]](kvs : mutable.Map[String, Any])( implicit override val M: Monad[M]) extends RepositoryEnvironment[M] {

  override def insert[A](key: String, value: A): M[Unit] = M.pureEval( Now {
    kvs += key -> value
    ()
  })

  override def get[A](key: String):  M[Option[A]] = M.pureEval( Now {
    kvs.get(key).map(_.asInstanceOf[A])
  })

  override def delete(key: String): M[Unit] = M.pureEval( Now {
    kvs.remove(key)
    ()
  })

  override def put[A](key: String, value: A): M[Unit] = M.pureEval( Now {
    kvs(key) = value
    ()
  })

  override def listAll[A]: M[List[A]] = M.pureEval( Now {
    kvs.values.toList.map(_.asInstanceOf[A])
  })


}
