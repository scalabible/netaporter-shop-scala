package com.netaporter.repositories

import cats.data.{Kleisli, ReaderT}
import cats.Monad
import cats.std.list._
import cats.syntax.traverse._
import com.netaporter.product.NetaporterProduct

trait ProductRepository[M[_]] {
  def M: Monad[M]
  def saveProduct(productId: String, product: NetaporterProduct): ReaderT[M, RepositoryEnvironment[M], Unit]
  def getProduct(productId: String): ReaderT[M, RepositoryEnvironment[M], Option[NetaporterProduct]]
  def saveMultipleProduct(products: List[NetaporterProduct]): ReaderT[M, RepositoryEnvironment[M], List[Unit]]
  def listAllProducts : ReaderT[M, RepositoryEnvironment[M], List[NetaporterProduct]]
}

case class NetaporterProductRepository[M[_]]( implicit override val M: Monad[M]) extends ProductRepository[M] {
  override def saveProduct(productId: String, product: NetaporterProduct): ReaderT[M, RepositoryEnvironment[M], Unit] = Kleisli {
    (env) =>
      {
       env.insert(productId, product)
      }
  }

  override def getProduct(productId: String): ReaderT[M, RepositoryEnvironment[M], Option[NetaporterProduct]] = Kleisli {
    (env) =>
    {
     env.get(productId)
    }
  }

  override def saveMultipleProduct(products: List[NetaporterProduct]): ReaderT[M, RepositoryEnvironment[M], List[Unit]] = Kleisli{
    (env) =>
    {
      products.map(product => env.insert(product.productId, product)).sequenceU
    }
  }

  override def listAllProducts: ReaderT[M, RepositoryEnvironment[M], List[NetaporterProduct]] = Kleisli {
    (env) =>
      {
        env.listAll
      }
  }


}
