package com.netaporter.shopping

import cats.{Now, Monad,Eval}
import cats.data.{Kleisli, ReaderT, Xor}
import com.netaporter.exception.{ProductNotExistInBasket, ProductNotExist, AppError}
import com.netaporter.repositories.{NetaporterProductRepository, RepositoryEnvironment}
import com.netaporter.product.NetaporterProduct
import scala.collection.mutable

trait ShoppingBasket [M[_]] {
  def addProductToBasket(productId: String): ReaderT[M, RepositoryEnvironment[M], Xor[AppError, NetaporterProduct]]
  def deleteFromBasket(productId: String): ReaderT[M, RepositoryEnvironment[M], Xor[AppError, String]]
  def totalCost: ReaderT[M, RepositoryEnvironment[M], Double]
}

case class NetaporterShoppingBasket[M[_]: Monad](
  kvs: mutable.Map[String, Double],
  productRepo: NetaporterProductRepository[M]
  ) extends ShoppingBasket[M] {
  override def addProductToBasket(productId: String): ReaderT[M, RepositoryEnvironment[M], Xor[AppError, NetaporterProduct]] = for {
    result <- productRepo.getProduct(productId).flatMap{
      _ match {
        case Some(product) => Kleisli.pure[M, RepositoryEnvironment[M], Xor[AppError, NetaporterProduct]]{
          kvs += productId -> product.price
          Xor.Right(product)}
        case None => Kleisli.pure[M, RepositoryEnvironment[M], Xor[AppError, NetaporterProduct]]{Xor.Left(ProductNotExist(productId: String))}
      }
    }
  } yield result

  override def deleteFromBasket(productId: String): ReaderT[M, RepositoryEnvironment[M], Xor[AppError, String]] = for {
    result <- kvs.get(productId) match {
      case Some(product) => Kleisli.pure[M, RepositoryEnvironment[M], Xor[AppError, String]]{
        kvs.remove(productId)
        Xor.Right(productId)}
      case None =>  Kleisli.pure[M, RepositoryEnvironment[M], Xor[AppError, String]]{Xor.Left(ProductNotExistInBasket(productId: String))}
    }
  } yield result

  override def totalCost: ReaderT[M, RepositoryEnvironment[M], Double] = {
    Kleisli.pure[M, RepositoryEnvironment[M], Double]{
      kvs.values.toList.sum
    }
  }
}
