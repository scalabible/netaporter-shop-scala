package com.netaporter.helpers

import com.netaporter.exception._
import com.netaporter.product.NetaporterProduct

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import cats.data.{ReaderT, Xor}

object ResponseHandler {

  def addProduct(result: Future[Xor[AppError, NetaporterProduct]]): Future[Unit] = {
    result.map {
      _ match {
        case Xor.Right(product) => println(s"product successfully added to basket with id :${product.productId}")
        case Xor.Left(error) => handleError(error)
      }
    }
  }

    def deleteProduct(result: Future[Xor[AppError, String]]): Future[Unit] = {
      result.map {
        _ match {
          case Xor.Right(id) => println(s"product successfully deleted basket with id:$id")
          case Xor.Left(error) => handleError(error)
        }
      }
    }

  def handleError(error: AppError): Unit = {
    error match {
      case ParseException(throwable) => println("sorry a parsing exception has occur")
      case IOException(throwable) => println("sorry a parsing exception has occur")
      case NoSuchResource(path: String) => println(s"$path: This resource does not exist")
      case ProductNotExist(productId: String) => println(s"product with product id : $productId does not exist in database")
      case ProductNotExistInBasket(productId: String) => println(s"This product does not exist in basket $productId and can not be deleted")
      case NoHeader => println("product data contains no header")
    }
  }
}
