package com.netaporter

import cats._
import cats.std.future._
import cats.data.Xor
import com.netaporter.helpers.ResponseHandler
import com.netaporter.product.ProductData
import com.netaporter.repositories.NetaporterRepositoryEnvironment
import com.netaporter.repositories.NetaporterProductRepository
import com.netaporter.shopping.NetaporterShoppingBasket
import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object Application extends App {

  val productUrlPath = "/items.csv"

  //key-value stores
  val productDatabase = mutable.Map.empty[String, Any]
  val shoppingBasket = mutable.Map.empty[String, Double]

  //repositories
  val env = NetaporterRepositoryEnvironment[Future](productDatabase)
  val productRepo = NetaporterProductRepository[Future]
  val shoppingBasketRepo = NetaporterShoppingBasket[Future](shoppingBasket,productRepo)

  val productData = ProductData(productRepo)
  val listOfProducts =  productData.getListOfProducts(productUrlPath)

  println("************************************")
  println("* Welcome to the Net-A-Porter Shop *")
  println("************************************")
  println("Enter \"Q\" to Quit")
  println("Enter \"add <ProductId>\" to add to basket")
  println("Enter \"remove <ProductId>\" to remove from basket")
  println("Enter \"list\" to show a list of products in the inventory")
  println("Enter \"total\" to show the total price of the basket")


 val result =  productData.uploadListOfProductToDatabase(listOfProducts).run(env).map{
    _ match {
      case Xor.Right(value) => {

        val input:Iterator[List[String]]  = io.Source.stdin.getLines.takeWhile(!_.equals("Q")).map(_.split(" ").toList)
          //val input = Iterator(List("add ", "1"), List("total"))
          input foreach {
          case "add " :: productId :: Nil =>
            val result = ResponseHandler.addProduct(shoppingBasketRepo.addProductToBasket(productId).run(env))
            Await.result(result, 5 seconds)

          case "remove" :: productId :: Nil =>
            val result = ResponseHandler.deleteProduct(shoppingBasketRepo.deleteFromBasket(productId).run(env))
            Await.result(result, 5 seconds)

          case "list" :: Nil =>
            val result = productRepo.listAllProducts.run(env).map{product =>
              product.foreach(p => println(s"${p.price}, ${p.ProductName}, ${p.price}"))
            }
            Await.result(result, 5 seconds)

          case "total" :: Nil =>
            val result = shoppingBasketRepo.totalCost.run(env).map(cost => println(s"Total cost = dollars ${cost}"))
            Await.result(result, 5 seconds)

          case _ =>
            println(s"Sorry, that is not a valid command")
        }

      }
      case Xor.Left(error) => s"error occur uploading product date to database ${ResponseHandler.handleError(error)}"
    }
  }

  Await.result(result, 6 seconds)







}
