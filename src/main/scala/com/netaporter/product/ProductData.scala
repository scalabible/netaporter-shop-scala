package com.netaporter.product

import com.netaporter.exception._
import java.nio.file.{Files, Paths}
import com.netaporter.repositories.{RepositoryEnvironment, NetaporterProductRepository}
import collection.JavaConverters._
import cats.{Now, Monad,Eval}
import cats.data.{Kleisli, ReaderT, Xor}


case class ProductData[M[_]: Monad](productRepo: NetaporterProductRepository[M]) {

  def readAllLines(path: String): Xor[AppError, Stream[String]] = {
      for {
        r <- Xor.fromOption(Option(this.getClass.getResource(path)), NoSuchResource(path))
        x <- Xor.catchNonFatal(Files.readAllLines(Paths.get(r.toURI))).leftMap(e => IOException(e))
      } yield  x.asScala.toStream
  }

  case class CsvHolder(indices:Map[String, Int], rows: Stream[String]) {
    def parse:Xor[ AppError, List[NetaporterProduct]] = {
      Xor.catchNonFatal(rows.toList.map{ line =>
        val cells = line.split(",")
        NetaporterProduct(cells(indices("#productId")).trim, cells(indices("#productName")).trim, cells(indices("#price")).trim.filterNot(c => c == '$').toDouble)
      }).leftMap(e => ParseException(e))
    }
  }

  def toCsvHolder(lines: Stream[String]): Xor[ AppError, CsvHolder] = {
     lines match {
       case Stream.Empty => Xor.left(NoHeader)
       case header #:: data => Xor.right(CsvHolder(header.split(",").toStream.map(_.trim).zipWithIndex.toMap, data))
     }
  }

  def getListOfProducts(path: String): Xor[AppError, List[NetaporterProduct]] = {
    for{
      lines <- readAllLines(path)
      csvHolder <- toCsvHolder(lines)
      products <- csvHolder.parse
   }yield products
  }

  def uploadListOfProductToDatabase(products: Xor[AppError, List[NetaporterProduct]]): ReaderT[M, RepositoryEnvironment[M], Xor[AppError ,List[Unit]]] = {
    products match {
      case Xor.Right(products) => productRepo.saveMultipleProduct(products).map(x => Xor.Right(x))
      case Xor.Left(error) => Kleisli.pure[M, RepositoryEnvironment[M], Xor[AppError ,List[Unit]]] { Xor.Left(error) }
    }
  }

}
