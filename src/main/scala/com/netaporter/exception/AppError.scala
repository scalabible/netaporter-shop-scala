package com.netaporter.exception


sealed trait AppError

case class ParseException(t: Throwable) extends AppError
case class NoSuchResource(path: String) extends AppError
case class IOException(underlying: Throwable) extends AppError
case class ProductNotExist(productId: String) extends  AppError
case class ProductNotExistInBasket(productId: String) extends AppError
case object NoHeader extends AppError
