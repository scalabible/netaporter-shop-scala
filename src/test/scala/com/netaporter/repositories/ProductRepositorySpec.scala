package com.netaporter.repositories

import cats.Id
import com.netaporter.product.NetaporterProduct
import org.specs2.Specification
import scala.collection.mutable

class ProductRepositorySpec extends Specification {
  def is = sequential ^
    s2"""
    ProductRepository
    ======================

      saveProduct
        simple save product $executeSaveProduct
      getProduct
        simple get no result in database $executeGetProductNone
        simple get One result in database $executeGetProductOne
      saveProduct
         save multiple product $executeSaveMultipleProduct
    """

  val netporterProductRepository = NetaporterProductRepository[Id]()
  def executeSaveProduct = {
    val kvs = mutable.Map.empty[String, Any]
    val env = NetaporterRepositoryEnvironment[Id](kvs)
    val product = NetaporterProduct("1","Short Sleeve Jumper",9.99)
    val result = netporterProductRepository.saveProduct("1", product).run(env)
    (result must beEqualTo(())) and
      (kvs("1") must beEqualTo(product))
  }

  def executeGetProductNone = {
    val kvs = mutable.Map.empty[String, Any]
    val env = NetaporterRepositoryEnvironment[Id](kvs)
    val result = netporterProductRepository.getProduct("10").run(env)
    result must beEqualTo(None)
  }

  def executeGetProductOne = {
    val kvs = mutable.Map[String, Any]("1" -> NetaporterProduct("1","Short Sleeve Jumper",9.99))
    val env = NetaporterRepositoryEnvironment[Id](kvs)
    val product = NetaporterProduct("1","Short Sleeve Jumper",9.99)
    val result = netporterProductRepository.getProduct("1").run(env)
    result must beEqualTo(Some(product))
  }

  def executeSaveMultipleProduct = {
    val kvs = mutable.Map.empty[String, Any]
    val env = NetaporterRepositoryEnvironment[Id](kvs)
    val products = List(NetaporterProduct("1","Short Sleeve Jumper",9.99), NetaporterProduct("2","T shirt",20.00))
    val result = netporterProductRepository.saveMultipleProduct(products).run(env)
    (result must beEqualTo(List((), ()))) and
      (kvs("1") must beEqualTo(products.head)) and
      (kvs("2") must beEqualTo(products(1)))
  }
}
