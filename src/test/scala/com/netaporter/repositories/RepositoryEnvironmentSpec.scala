package com.netaporter.repositories

import cats.Id
import org.specs2.Specification
import scala.collection.mutable

class RepositoryEnvironmentSpec extends Specification{
  def is = sequential ^
    s2"""
    RepositoryEnvironment
    ======================

      Insert
        simple insert $executeInsert
      Get
        simple get no result in database $executeGetOne
        simple get One result in database $executeGetTwo
      put
        simple pull $executePut
      delete
        simple delete $executeDelete
    """


  val kvs = mutable.Map.empty[String, Any]
  def executeInsert = {
    val env = NetaporterRepositoryEnvironment[Id](kvs)
    val result = env.insert("1", 1)
    (result must beEqualTo(())) and
      (kvs("1") must beEqualTo(1))
  }

  def executeGetOne = {
    val env = NetaporterRepositoryEnvironment[Id](kvs)
    val result = env.get("3")
    result must beEqualTo(None)
  }

  def executeGetTwo = {
    val env = NetaporterRepositoryEnvironment[Id](kvs)
    val result = env.get("1")
    result must beEqualTo(Some(1))
  }

  def executePut = {
    val env = NetaporterRepositoryEnvironment[Id](kvs)
    val result = env.put("1", 7)
    (result must beEqualTo(())) and
      (kvs("1") must beEqualTo(7))
  }

  def executeDelete = {
    val env = NetaporterRepositoryEnvironment[Id](kvs)
    val result = env.delete("1")
    (result must beEqualTo(())) and
      (kvs.get("1") must beEqualTo(None))
  }
}
