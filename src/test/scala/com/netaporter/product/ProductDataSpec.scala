package com.netaporter.product

import cats.Id
import cats.data.{Xor}
import com.netaporter.exception.NoHeader
import com.netaporter.exception.NoSuchResource
import com.netaporter.repositories.NetaporterProductRepository
import org.specs2.Specification

class ProductDataSpec extends Specification  {
  def is = sequential ^
    s2"""
    ProductData
    ======================
      readAllLines
         for non existing path should return error $underTestOne
         for existing path should return a stream $underTestTwo
      toCSvHolder
         for a non empty stream should return a CSVHolder $underTestThree
         for a empty stream should return AppError object $underTestFour
    """

  val productRepo = NetaporterProductRepository[Id]()
  val productData = ProductData[Id](productRepo)

  def underTestOne = {
    val path = "/noExistPath"
    val result = productData.readAllLines(path)
    result must beEqualTo(Xor.Left(NoSuchResource(path)))

  }

  def underTestTwo = {
    val path = "/items.csv"
    val result = productData.readAllLines(path)
    result.isRight must beEqualTo(true)
  }


  def underTestThree = {
    val lines = List("#productId,#productName,#price",
      "1,Short Sleeve Jumper,9.99",
      "2,Shoulder Bag,9.99",
      "3,Skinny Jeans,45.00",
      "4,Leather Jeans,80",
      "5,Leather Jacket,199.99",
      "6,Wool Socks ,20.50",
      "7,Pique Polo shirt, 50.55").toStream

    val result = productData.toCsvHolder(lines)
    result.isRight must beEqualTo(true)
  }

  def underTestFour = {
    val lines = Stream.empty[String]
    val result = productData.toCsvHolder(lines)
    result must beEqualTo(Xor.Left(NoHeader))
  }

}
