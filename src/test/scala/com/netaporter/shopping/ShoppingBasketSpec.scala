package com.netaporter.shopping


import cats.Id
import com.netaporter.exception.ProductNotExistInBasket
import com.netaporter.product.NetaporterProduct
import com.netaporter.repositories.NetaporterRepositoryEnvironment
import com.netaporter.repositories.NetaporterProductRepository
import org.specs2.Specification
import cats.data.{Kleisli, Xor}

import scala.collection.mutable

class ShoppingBasketSpec extends Specification {
  def is = sequential ^
    s2"""
    ShoppingBasket
    ======================

      addProductToBasket
        simple save product $executeAddProductToBasket
      deleteProductFromBasket
        delete product from basket, product does not exist $executeDeleteProductFromBasketTestOne
        delete product from, product exist $executeDeleteProductFromBasketTestTwo
      totalCost
        get total, no product in basket $executeTotalCostOne
        get total multiple item in basket $executeTotalCostTwo
    """

  val productDatabase = mutable.Map[String, Any](
    "1" -> NetaporterProduct("1","Short Sleeve Jumper",9.99),
    "2" -> NetaporterProduct("1","Short Sleeve Jumper",9.99)
  )

  val productRepo = NetaporterProductRepository[Id]
  val env = NetaporterRepositoryEnvironment[Id](productDatabase)
  def executeAddProductToBasket = {

    val shoppingBasket = mutable.Map.empty[String, Double]
    val netaporterShoppingBasket = NetaporterShoppingBasket[Id](shoppingBasket,productRepo)
    val result = netaporterShoppingBasket.addProductToBasket("1").run(env)
    (result must beEqualTo(Xor.Right(NetaporterProduct("1","Short Sleeve Jumper",9.99)))) and
      (shoppingBasket("1") must beEqualTo(9.99))
  }

  def executeDeleteProductFromBasketTestOne = {
    val shoppingBasket = mutable.Map.empty[String, Double]
    val netaporterShoppingBasket = NetaporterShoppingBasket[Id](shoppingBasket,productRepo)
    val result = netaporterShoppingBasket.deleteFromBasket("1").run(env)
    result must beEqualTo(Xor.Left(ProductNotExistInBasket("1")))
  }

  def executeDeleteProductFromBasketTestTwo = {
    val shoppingBasket = mutable.Map[String, Double]("1" -> 9.99)
    val netaporterShoppingBasket = NetaporterShoppingBasket[Id](shoppingBasket,productRepo)
    val result = netaporterShoppingBasket.deleteFromBasket("1").run(env)
    (result must beEqualTo(Xor.Right("1"))) and
      (shoppingBasket.get("1") must beEqualTo(None))
  }

  def executeTotalCostOne = {
    val shoppingBasket = mutable.Map.empty[String, Double]
    val netaporterShoppingBasket = NetaporterShoppingBasket[Id](shoppingBasket,productRepo)
    val result = netaporterShoppingBasket.totalCost.run(env)
    result must beEqualTo(0)
  }

  def executeTotalCostTwo = {
    val shoppingBasket = mutable.Map[String, Double]("1" -> 9.99, "2" -> 20.00, "3" -> 10.00)
    val netaporterShoppingBasket = NetaporterShoppingBasket[Id](shoppingBasket,productRepo)
    val result = netaporterShoppingBasket.totalCost.run(env)
    result must beEqualTo(39.99)
  }
}

